import tensorflow as tf
import sys
import numpy as np
import re, json
from data_utils import postprocess

def load_vocab(fp):
    word2ix = json.load(open(fp, 'r'))

    # prepare ix2word
    ix2word = dict(src=None, target=None)
    ix2word['src'] = dict(zip(word2ix['src'].values(), word2ix['src'].keys()))
    ix2word['src_subword'] = dict(zip(word2ix['src_subword'].values(), word2ix['src_subword'].keys()))
    ix2word['target'] = dict(zip(word2ix['target'].values(), word2ix['target'].keys()))

    return word2ix, ix2word

def llprint(message):
    sys.stdout.write(message)
    sys.stdout.flush()

def convert_to_sent(sent, ix2word, is_postprocess):
    if isinstance(sent,np.ndarray):
        sent = ' '.join([ ix2word['target'].get(i,'<unk>') for i in sent[0] ])
    sent = re.sub(r'<end>.*', '', sent)

    if is_postprocess:
        # postprocessing
        sent = postprocess(target_sent=sent)

    return sent

def RNNCellWrapper(num_units, num_layers, keep_prob, attention=False, attention_mechanism=None, attention_layer_size=None):
    cells = []
    for _ in range(num_layers):
        cell = tf.nn.rnn_cell.GRUCell(num_units=num_units)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)
        cells.append(cell)

    cell = tf.nn.rnn_cell.MultiRNNCell(cells)

    if attention:
        # decoder cell with attention
        cell = tf.contrib.seq2seq.AttentionWrapper(cell=cell, attention_mechanism=attention_mechanism,
                                                   attention_layer_size=attention_layer_size)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)

    return cell


def entry_stop_gradients(embedding, mask):
    mask_h = tf.logical_not(mask)

    mask = tf.cast(mask, dtype=embedding.dtype)
    mask_h = tf.cast(mask_h, dtype=embedding.dtype)

    return tf.stop_gradient(mask_h * embedding) + mask * embedding
